clear
tic
warning off
addpath('../../../com/ExternalPackages/KaijunWang_NCestimation')
addpath('C:\WorkSpace\Matlab\Projects\FromRenato');



for runCase=[-2,13:24]
    
    
    DATA_FILE_PATH='';
    RESULT_PATH='';
        
    
     if runCase == -2
        DATA_FILE_PATH = '../../../Projects/SyntheticData/DataSetsNoNoise/Cov05'
        RESULT_PATH='../Results/Cov05/NoNoise';
     elseif runCase == -1
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\DataSetsNoNoise\Real'
        RESULT_PATH='../Results/Real/NoNoise';
    elseif runCase == 1
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Uniform\FeatureAdd\FullNoise';
        RESULT_PATH='../Results/REAL\Uniform\FeatureAdd\FullNoise';
    elseif  runCase == 2
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Uniform\FeatureAdd\HalfNoise';
        RESULT_PATH='../Results/REAL\Uniform\FeatureAdd\HalfNoise';
    elseif    runCase == 3
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Uniform\FeatureBlur\FullNoise';
        RESULT_PATH='../Results/REAL\Uniform\FeatureBlur\FullNoise';
    elseif  runCase == 4
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Uniform\FeatureBlur\HalfNoise';
        RESULT_PATH='../Results/REAL\Uniform\FeatureBlur\HalfNoise';
    elseif    runCase == 5
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Uniform\FeatureBlurCluster\FullNoise';
        RESULT_PATH='../Results/REAL\Uniform\FeatureBlurCluster\FullNoise';
    elseif  runCase == 6
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Uniform\FeatureBlurCluster\HalfNoise';
        RESULT_PATH='../Results/REAL\Uniform\FeatureBlurCluster\HalfNoise';
        
    elseif runCase == 7
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Gaussian\FeatureAdd\FullNoise';
        RESULT_PATH='../Results/REAL\Gaussian\FeatureAdd\FullNoise';
    elseif  runCase == 8
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Gaussian\FeatureAdd\HalfNoise';
        RESULT_PATH='../Results/REAL\Gaussian\FeatureAdd\HalfNoise';
    elseif    runCase == 9
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Gaussian\FeatureBlur\FullNoise';
        RESULT_PATH='../Results/REAL\Gaussian\FeatureBlur\FullNoise';
    elseif  runCase == 10
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Gaussian\FeatureBlur\HalfNoise';
        RESULT_PATH='../Results/REAL\Gaussian\FeatureBlur\HalfNoise';
    elseif    runCase == 11
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Gaussian\FeatureBlurCluster\FullNoise';
        RESULT_PATH='../Results/REAL\Gaussian\FeatureBlurCluster\FullNoise';
    elseif  runCase == 12
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\REAL\Gaussian\FeatureBlurCluster\HalfNoise';
        RESULT_PATH='../Results/REAL\Gaussian\FeatureBlurCluster\HalfNoise';
        
        
        
        
        
        
        
    elseif runCase == 13
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Uniform\FeatureAdd\FullNoise';
        RESULT_PATH='../Results/Cov05\Uniform\FeatureAdd\FullNoise';
    elseif  runCase == 14
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Uniform\FeatureAdd\HalfNoise';
        RESULT_PATH='../Results/Cov05\Uniform\FeatureAdd\HalfNoise';
    elseif    runCase == 15
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Uniform\FeatureBlur\FullNoise';
        RESULT_PATH='../Results/Cov05\Uniform\FeatureBlur\FullNoise';
    elseif  runCase == 16
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Uniform\FeatureBlur\HalfNoise';
        RESULT_PATH='../Results/Cov05\Uniform\FeatureBlur\HalfNoise';
    elseif    runCase == 17
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Uniform\FeatureBlurCluster\FullNoise';
        RESULT_PATH='../Results/Cov05\Uniform\FeatureBlurCluster\FullNoise';
    elseif  runCase == 18
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Uniform\FeatureBlurCluster\HalfNoise';
        RESULT_PATH='../Results/Cov05\Uniform\FeatureBlurCluster\HalfNoise';
        
    elseif runCase == 19
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Gaussian\FeatureAdd\FullNoise';
        RESULT_PATH='../Results/Cov05\Gaussian\FeatureAdd\FullNoise';
    elseif  runCase == 20
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Gaussian\FeatureAdd\HalfNoise';
        RESULT_PATH='../Results/Cov05\Gaussian\FeatureAdd\HalfNoise';
    elseif    runCase == 21
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Gaussian\FeatureBlur\FullNoise';
        RESULT_PATH='../Results/Cov05\Gaussian\FeatureBlur\FullNoise';
    elseif  runCase == 22
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Gaussian\FeatureBlur\HalfNoise';
        RESULT_PATH='../Results/Cov05\Gaussian\FeatureBlur\HalfNoise';
    elseif    runCase == 23
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Gaussian\FeatureBlurCluster\FullNoise';
        RESULT_PATH='../Results/Cov05\Gaussian\FeatureBlurCluster\FullNoise';
    elseif  runCase == 24
        DATA_FILE_PATH = 'C:\WorkSpace\Matlab\Projects\SyntheticData\NoiseData\Cov05\Gaussian\FeatureBlurCluster\HalfNoise';
        RESULT_PATH='../Results/Cov05\Gaussian\FeatureBlurCluster\HalfNoise'; 
    end
    
    
    dbFiles = dir(strcat(DATA_FILE_PATH,'/*.mat'));
    FILE_N = size(dbFiles,1)
    
    if runCase == -2
        DB_N = 20;
    elseif runCase<=12
        DB_N = 1;
    else
        DB_N = 20;
     end
    
    
     
    %DB_N = 1;
    %% hold information of all of the index..
    %% for each 12 by 50 dsIndex the all_index_col contain a cell, each cell having the following structure..
    %%%[ fileIndex dataIndex N M NF index.....]
    % contain database info
    %       number of entities,
    %       Number of features,
    %       number of noisy features,
    %       number of clusters,
    %       noise type (0,1,2): 0 no noise, 1 half noise, 2 full noise,
    %       number of databse
    
    
    %% number of the experiment (case) considered
    %EXPERIMENT_NUMBER = 8;
    
    %%collection of Mitra
    %MITRA_RESULT_COL = cell(12,50);
    
    %ADD_MAX_FEATURE = true;
    
    
    % in the data file]
    DATABASE_INFO_COL = zeros(FILE_N,6);
    
    %% store rand index for each processing for k known
    RAND_INDEX_COL = cell(FILE_N,DB_N);
    %% store the feature index from each processing for k known
    FEATURE_FILTER_COL = cell(FILE_N,DB_N);
    
    
    
    for fileIndex = 1: FILE_N
        FileName = dbFiles(fileIndex).name(1,1:find(dbFiles(fileIndex).name=='.')-1);
        % cd(DATA_FILE_PATH);
        load(strcat(DATA_FILE_PATH,'/',FileName,'.mat'));
        
        try
            Data = NoiseData;
        catch
        end
        
        dsNumber = size(Data,2);
        [N,M] = size(Data{1,1});
        K = max(Data{2,1});
        %    NF = 0;
        
        
        %     if size(strList,2)==6
        %         noiseFeature = strsplit(char(strList(1,5)),'N');
        %         NF = str2double(noiseFeature(1,1));
        %         % clear noiseFeature
        %     end
        
        
        %% store the current data file information...
        %    DATABASE_INFO_COL(fileIndex,:) = [N M NF K (NF/M)*2 dsNumber];
        
        if runCase > 12 || runCase == -2
            dsNumber = 20; 
        end
    
        
        for dsIndex = 1 :dsNumber
            
            data = Data{1,dsIndex};% [Data{1,dsIndex} noiseData];
            label = Data{2,dsIndex};
            
            
           % for pInd=2:
            %% create a local variable to store the rand index and feature filter index obatined from each experiments
            %% we have 3 experiment in this case
            currRandIndex = zeros(2,1);
            currFeatureFilter = zeros(2,M);
            
            %% create a local variable to store the rand index and feature filter index obatined from each experiments
            %% we have 4 experiment in this case...
            
            
            
            
            %currRandIndex_CRecovery
            
            
            fprintf('Run Case : %d  Current File %d, data set %i out of %i - \n', runCase,fileIndex, dsIndex, dsNumber);
          
            
            
            %% assume we know number of cluster K.
            [U, FinalW, ~] = SubWIkMeans(data, 1, p, K, p, false);
            fprintf('\nSubWIKMeans called....');
            
            
            
            
            %% Experiment 1:  case  consiser the average weight
            
            %% the features having the average weight per clusters greater than or equal to 1/(M) are
            %% the relevant features....
            %% get the average weight
            avgW = mean(FinalW);
            maxW = max(FinalW);
            %% find the feature index greater than or equal to 1/(M)
            [~, fIndexAvg] = find(avgW >= 1/(M));
            [~, fIndexMax] = find(maxW >= 1/(M));
            %% convert the index into filter by bitmapping the fIndex with 0 and 1.
            fFilterAvg = zeros(1,M);
            fFilterMax = zeros(1,M);
            
            fFilterAvg(fIndexAvg)=1;            fFilterMax(fIndexMax)=1;
            
            %%conver to logical
            fFilterAvg = fFilterAvg ==1;
            fFilterMax = fFilterMax ==1;
            %% get the rand Index to check the cluster recovery...
            %% here we use kmeans, we consider we know K ...
            [Umean,~] = kmeans(data(:,fFilterAvg),K,'Replicates',100);
            [Umax,~] = kmeans(data(:,fFilterMax),K,'Replicates',100);
            %% get the rand index
            currRandIndex(1,1)=RandIndex(label,Umean);
            currRandIndex(2,1)=RandIndex(label,Umax);
            %% save the feature filter
            currFeatureFilter(1,:)  = fFilterAvg;
            currFeatureFilter(2,:)  = fFilterMax;
            % % clear avgW U fFilterAvg
            
            
            % clear iKFS_FIndex  iKFS_Filter U_7
            
            RAND_INDEX_COL{fileIndex,dsIndex} = currRandIndex;
            %% store the feature index from each processing
            FEATURE_FILTER_COL{fileIndex,dsIndex} = currFeatureFilter;
            
            % clear currRandIndex currFeatureFilter
            runTime = toc;
            
            % clear data label FinalW
        end
        save(strcat(RESULT_PATH,'/fsfm.mat'),'RAND_INDEX_COL','FEATURE_FILTER_COL','runTime');
        % clear N NF M K fileIndex  FileName strList dataFeature dsNumber dsIndex Data
    end
    %% % clear storage
    % clear DATABASE_INFO_COL FEATURE_FILTER_COL_SUP RAND_INDEX_COL_SUP
    %% % clear global variable
    % clear dbFiles CorrectK
    %% % clear constants
    % clear DATA_FILE_PATH OUT_DIR_PATH EXPERIMENT_NUMBER
end
    
