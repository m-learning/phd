%%test with GAUSSIAN
clear
addpath('../util')
% addpath('E:\WorkSpace\Matlab\Projects\SyntheticData\algorithms');
% 
% addpath('../../../com/ExternalPackages/KaijunWang_NCestimation');
% addpath('../../../com/ExternalPackages/MitraFeatureSelection');
% addpath('E:\WorkSpace\Matlab\Projects\ReverseNN\MissingValueProject2018\algorithms')
% addpath('E:\WorkSpace\Matlab\Projects\FromRenato');
% dbFiles = dir('GMM_DATA\*.mat');
% 
% N=1000;
 pList=[1.1,1.5,2,2.5,3,3.5,4,4.5,5];
% DB_N=10;
% 
noiseIndex=[0.1,0.2,0.3,0.4];
% 
% 
% 
% 
% 
% allResults=[];
% 
% 
% allFS={};
% 
% 
% 
% 
% 
% 
% 
% for fIndex=1:size(dbFiles,1)
%     load(strcat('GMM_DATA\',dbFiles(fIndex).name));
%     FileName = dbFiles(fIndex).name;
%     for dbIndex = 1:DB_N
%         
%         
%         currDB=Data{1,dbIndex};
%         currU = Data{2,dbIndex};
%         currAngle = getAngle(currDB,2);
%          M=size(currDB,2);
%         
%         K = max(Data{2,1});
%         
%         
%         
%         %% All
%         pARI_Mean=cell(size(pList,2),3);
%         pIndex_Mean=cell(size(pList,2),3);
%         
%         pARI_Max=cell(size(pList,2),3);
%         pIndex_Max=cell(size(pList,2),3);
%         
%         
%         
%         %%% Best
%         pBEST_ARI_Mean=zeros(size(pList,2),3);
%         pBEST_Index_Mean=cell(size(pList,2),3);
%         
%         pBEST_ARI_Max=zeros(size(pList,2),3);
%         pBEST_Index_Max=cell(size(pList,2),3);
%         
%         
%         
%         
%         
%         
%         
%         
%         
%         pSIL_ARI_Mean=zeros(size(pList,2),3);
%         pSIL_Index_Mean=cell(size(pList,2),3);
%         
%         
%         pSIL_ARI_Max=zeros(size(pList,2),3);
%         pSIL_Index_Max=cell(size(pList,2),3);
%         
%         
%         
%         
%         
%         
%         
%         
%         
%         
%         
%         
%         
%         
%         tmpCol={};
%         
%         
%         for pIndex=1:size(pList,2)
%             
%             
%             
%            
%             
%             
%             for noiseType=1:size(noiseIndex,2)
%                
%                 currDBNoise=currDB;
%                 noiseN=ceil(1000*noiseIndex(1,noiseType));
%                 nList =  randperm(1000,noiseN);
%                 
%                 
%                 %%adding noise
%                 nList =  randperm(1000,noiseN);
%                 currDBNoise(nList,1)=nan;
%                 nList =  randperm(1000,noiseN);
%                 currDBNoise(nList,2)=nan;
%                 
%                 %%% mean average imputation
%                 currDBNoiseImpu=currDBNoise;
%                 currDBNoiseImpu(isnan(currDBNoiseImpu(:,1)),1)=nanmean(currDBNoiseImpu(:,1));
%                 currDBNoiseImpu(isnan(currDBNoiseImpu(:,2)),2)=nanmean(currDBNoiseImpu(:,2));
%                 
%                
%                 
%                 
%                 
%                 %% assume we know number of cluster K.
%                     [U, FinalW, ~] =MWKmeans(currDBNoiseImpu, K, pList(1,pIndex));
%                     
%                     
%                     
%                     %% Experiment 1:  case  consiser the average weight
%                     
%                     %% the features having the average weight per clusters greater than or equal to 1/(M) are
%                     %% the relevant features....
%                     %% get the average weight
%                     avgW = mean(FinalW);
%                     maxW = max(FinalW);
%                     %% find the feature index greater than or equal to 1/(M)
%                     [~, fIndexAvg] = find(avgW >= 1/(M));
%                     [~, fIndexMax] = find(maxW >= 1/(M));
%                     %% convert the index into filter by bitmapping the fIndex with 0 and 1.
%                     fFilterAvg = zeros(1,M);
%                     fFilterMax = zeros(1,M);
%                     
%                     fFilterAvg(fIndexAvg)=1;
%                     fFilterMax(fIndexMax)=1;
%                     
%                     %%conver to logical
%                     fFilterAvg = fFilterAvg ==1;
%                     fFilterMax = fFilterMax ==1;
%                     %% get the rand Index to check the cluster recovery...
%                     %% here we use kmeans, we consider we know K ...
%                     [Umean,~] = getStraightKMeanPD(currDBNoise(:,fFilterAvg),'K',max(currU));
%                     [Umax,~] = getStraightKMeanPD(currDBNoise(:,fFilterMax),'K',max(currU));
%                 
%                 
%                 
%                 
%                 %%%%%%%%%%%%%%  partial distance
%                 
%                 
%                  %% assume we know number of cluster K.
%                     [U, FinalW, ~] = MWKmeansPD(currDBNoise, K,pList(1,pIndex));
%                     
%                     
%                     
%                     %% Experiment 1:  case  consiser the average weight
%                     
%                     %% the features having the average weight per clusters greater than or equal to 1/(M) are
%                     %% the relevant features....
%                     %% get the average weight
%                     avgW = mean(FinalW);
%                     maxW = max(FinalW);
%                     %% find the feature index greater than or equal to 1/(M)
%                     [~, fIndexAvg1] = find(avgW >= 1/(M));
%                     [~, fIndexMax1] = find(maxW >= 1/(M));
%                     %% convert the index into filter by bitmapping the fIndex with 0 and 1.
%                     fFilterAvg1 = zeros(1,M);
%                     fFilterMax1 = zeros(1,M);
%                     
%                     fFilterAvg1(fIndexAvg1)=1;
%                     fFilterMax1(fIndexMax1)=1;
%                     
%                     %%conver to logical
%                     fFilterAvg1 = fFilterAvg1 ==1;
%                     fFilterMax1 = fFilterMax1 ==1;
%                     %% get the rand Index to check the cluster recovery...
%                     %% here we use kmeans, we consider we know K ...
%                     [Umean1,~] = getStraightKMeanPD(currDBNoise(:,fFilterAvg),'K',max(currU));
%                     [Umax1,~] = getStraightKMeanPD(currDBNoise(:,fFilterMax),'K',max(currU));
%                 
%                 tmpFS = [fFilterAvg;fFilterMax;fFilterAvg1;fFilterMax1];
%                 
%                 allResults=[allResults;fIndex,dbIndex,pList(1,pIndex),noiseIndex(1,noiseType) RandIndex(currU,Umean) RandIndex(currU,Umax ) RandIndex(currU,Umean1 ) RandIndex(currU,Umax1)];
%                   
%                 tmpCol{pIndex,noiseType}=tmpFS;
%                     
%                     
%             end
%         end
%         allFS{fIndex,dbIndex}=tmpCol;
%     end
% end
% 
% save('extendedToPD.mat','allResults','allFS')


load('extendedToPD.mat');

REPORT_PATH='ReportTex';
byP=[];
byPstd=[];
    for pIndex=1:size(pList,2)
        byP=[byP;mean(allResults(allResults(:,3)==pList(1,pIndex)  ,[3,5:end]))];
       
        
        byPstd=[byPstd;std(allResults(allResults(:,3)==pList(1,pIndex)  ,[3,5:end]))];
    end
    
byP=round(byP*1000)/1000;
byPStd=round(byPstd*100)/100;
DisplayP={};
for i=1:size(byP,1)
    [~,maxIndex] = max(byP(i,2:end));
    maxIndex=maxIndex+1;
    displayP{i,1}=num2str(byP(i,1));
    for j=2:size(byP,2)
        if sum(maxIndex==j)>0
            
            displayP{i,j}=strcat('\textbf{',num2str(byP(i,j)),'}');
        else
           displayP{i,j}=strcat(num2str(byP(i,j))); 
        end  
        displayP{i,j}=strcat(displayP{i,j},'/',num2str(byPStd(i,j)));
    end
end
    
    
byNoise=[];
byNoiseStd=[];

    for nIndex=1:size(noiseIndex,2)
        byNoise=[byNoise;mean(allResults(allResults(:,4)==noiseIndex(1,nIndex)  ,[4:end]))];
        byNoiseStd=[byNoiseStd;std(allResults(allResults(:,4)==noiseIndex(1,nIndex)  ,[4:end]))];

    end
 byNoise=round(byNoise*1000)/1000;   
 byNoiseStd=round(byNoiseStd*100)/100; 

displayN={};
for i=1:size(byNoise,1)
    [~,maxIndex] = max(byNoise(i,2:end));
    maxIndex=maxIndex+1;
    displayN{i,1}=num2str(byNoise(i,1));
    for j=2:size(byNoise,2)
        if sum(maxIndex==j)>0
            
            displayN{i,j}=strcat('\textbf{',num2str(byNoise(i,j)),'}');
        else
           displayN{i,j}=strcat(num2str(byNoise(i,j))); 
        end  
        displayN{i,j}=strcat(displayN{i,j},'/',num2str(byNoiseStd(i,j)));
    end
end
    
    
    
    targetLFilePath = strcat(REPORT_PATH,'\FSFS_extPD.tex')   
    tableCaption = 'Comparision of feature selection based on feature weighting algorithms with its extended partial distance. Peformance is measured based on RAND index and is grouped by distance coefficient and percentage noise. The Performance column contains  mean/std .';
   
    
    MultiHeadingLine1{1,1}={1,''};
    MultiHeadingLine1{1,2}={4,'Performance of Algorithm'};
    
    MultiHeadingLine3{1,1}={1,''};
    MultiHeadingLine3{1,2}={2,'average Imputation'};
    MultiHeadingLine3{1,3}={2,'Partial Distance'};
   
    
    
    MultiHeading={MultiHeadingLine1;MultiHeadingLine3};
    lineHNo=[1,2];
    heading = {'','\\textit{mean}FSFW','\\textit{max}FSFW','\\textit{mean}FSFWext\\textit{PD}','\\textit{max}FSFWext\\textit{PD}'};
    
    
    
   
   MRowInfo = {'Distance Coefficient','Percentage Noise'};
   generateLatexMRowTable( targetLFilePath,tableCaption,{displayP,displayN},MultiHeading,heading,lineHNo,MRowInfo,true)
    

