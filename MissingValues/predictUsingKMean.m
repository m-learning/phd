%[ missingData,missIndexCol_2,ABS_DIFF_INDEX(1,2),RAD_INDEX(1,2),RAND_INDEX(1,2)] = predictUsingKMean(dataWithMissVal,K,'missingListCol',missingListCol,'InitMethod',InitConstants.USE_AVERAGE,'label',label,'Version',1);
%[ missingData,missIndexCol_3,ABS_DIFF_INDEX(1,3),RAD_INDEX(1,3),RAND_INDEX(1,3)] = predictUsingKMean(dataWithMissVal,K,'missingListCol',missingListCol,'InitMethod',InitConstants.USE_AVERAGE,'label',label,'Version',2);
                
function [  tmpMissingData,missIndexCol,ABS_DIFF_INDEX,RAD_INDEX,RAND_INDEX ] = predictUsingKMean( missingData,K,varargin)


%%%%%%%%%%%%%%%%%%%%%%%%%%% PARAMETER SETUP %%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% parse variable.....
    %% get label, K and missingListCol from the parameters...
     for i=1:2:nargin-2
         varName = varargin{i};
         switch varName
             case 'label'
                 label = varargin{i+1};
             case 'missingListCol'
                 missIndexCol = varargin{i+1};       
             case 'initMethod'
                 initMethod = varargin{i+1};  
             case 'Version'
                 VERSION = varargin{i+1};
                % 'InitMethod',InitConstants.USE_AVERAGE
         end
     end

     if ~exist('VERSION', 'var')
         VERSION = 1;
     end
     
     if ~exist('initMethod','var')
         initMethod = InitConstants.USE_AVERAGE;
     end
    
 
     if ~exist('missIndexCol', 'var')
         MISS_COUNT = sum(sum(isnan(missingData)));
         missIndexCol = NaN(MISS_COUNT,3);
         [missIndexCol(:,1), missIndexCol(:,2)]= find(isnan(missingData));
     end
     
    
      
      
    
      if initMethod == InitConstants.USE_AVERAGE
         [ tmpMissingData,~ ] = predictUsingAverageVal(missingData);
      end
      
     
    
    
%%%%%%%%%%%%%%%%%%%%%%%% MAIN ALGORITHM %%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
    tmpMissingDataOld = tmpMissingData;
        %% find the Cluster 
     
        predU = kmeans(tmpMissingData,K,'Replicates',100);
        %% then replace the missing data with mean value within cluster....
   %     [mRowIndex, mColIndex]=find(isnan(missingData));
   %     kk=[];     
      %  for missIndex = 1:size(mRowIndex,1)
      for missIndex=1:size(missIndexCol,1)
            rN = missIndexCol(missIndex,1);
            rM = missIndexCol(missIndex,2);
            if VERSION == 1
                currCluster = tmpMissingData(predU==predU(rN,1),rM);
                tmpMissingData(rN,rM) = mean(currCluster);
   %             kk=[kk;[rN,rM, size(currCluster,1) missIndexCol(missIndex,3)  mean(currCluster)]];
            elseif VERSION == 2
                currCluster = tmpMissingDataOld(predU==predU(rN,1),rM);
                tmpMissingData(rN,rM) = nanmean(currCluster);
            end
            missIndexCol(missIndex,4)=nanmean(currCluster);     
      end
            
       
     %   kk = sortrows(kk,[1,2])
        
        
      
       
                
        

%         %% in version 2 there is chances that all of the attribute values withing the cluster are missing
%         %% Therefore, we chack if any of the predicted value is nan...
%         %% if yes, we repalce the value with average feature value.
%         
%         if VERSION == 2
%             if sum(sum(isnan(missingData))) > 0
%                 avgFeatureValue = nanmean(tmpMissingDataOld);
%                 [mRow mCol] = find(isnan(missingData));
%                 for mIndex=1:size(mRow,1)
%                     missingData(mRow(mIndex,1),mCol(mIndex,1))=avgFeatureValue(1,mCol(mIndex,1));
%                     missIndexCol(missIndexCol(:,1)==mRow(mIndex,1) & missIndexCol(:,2) == mCol(mIndex,1),4)= avgFeatureValue(1,mCol(mIndex,1));
%                     %% synchronize average feature value with each updata....
%                     avgFeatureValue = nanmean(missingData);
%                 end
%             end
%             tmpMissingData = missingData;
%         end
%         
        
        
        
        
%%%%% PERFORMANCE MEASUREMENT %%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
    %% find the RAD index by comparing the predict value with the orginal value given in missIndexCol [rowId,colId,actualValue]
    if ~isnan(missIndexCol(1,3))       
        ABS_DIFF_INDEX = sum(abs(missIndexCol(:,3)-missIndexCol(:,4)));
        RAD_INDEX = sum(abs((missIndexCol(:,3)-missIndexCol(:,4))./missIndexCol(:,3)))/size(missIndexCol,1);
    else
        ABS_DIFF_INDEX=NaN;
        RAD_INDEX = NaN;
    end
   
    %% if no label (cluster) is provided we cannot predict cluster recovery.......
    if exist('label', 'var')
        [U,~] = kmeans(tmpMissingData,max(label),'Replicates',100); 
      
        RAND_INDEX = RandIndex(label,U);
    else
        RAND_INDEX = NaN;
    end
  
 
end


