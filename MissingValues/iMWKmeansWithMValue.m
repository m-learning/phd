function [U, FinalW, InitW, FinalZ, InitZ, UDistToZ,LoopCount] = iMWKmeansWithMValue(Data, ikThreshold, p, MaxK)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%intelligent Minkowski Weighted K-Means
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%From:
%Amorim, R.C. and Mirkin, B., Minkowski Metric, Feature Weighting and Anomalous Cluster Initialisation in K-Means Clustering,
%Pattern Recognition, vol. 45(3), pp. 1061-1075, 2012.
%
%Parameters:
%Data
%      Dataset, format: Entities x Features
% ikThreshold
%       The intelligent K-Means threshold (theta). If the number of
%       clusters is known set this to zero.
%p
%      Distance and weight Exponent.
%MaxK (optional)
%       The maximum number of clusters
%
%
%Outputs
%
%U
%      Cluster Labels. Clearly they may not directly match the dataset
%      labels (you should use a confusion matrix).
%FinalW
%      Final Weights
%InitW
%      Initial Weights
%FinalZ
%      Final Centroids
%InitZ
%      Initial Centroids
%UDistToZ
%      The distance of each entity to its centroid.
%LoopCount
%      The number of loops the algorithm took to converge. The maximum is
%      hard-coded to 500 (variable MaxLoops)
Data = (Data - repmat(nanmean(Data),size(Data,1),1))./(repmat(nanmax(Data)-nanmin(Data),size(Data,1),1));
[Initial_Size_Data,N] = size(Data);
Weights = [];
QtdInCluster = [];
Centroids = [];
EqualWeights(1,1:N)=1/N;
InitialData = Data;

%Calculates the Minkowski Centre
MinkCentre(1,:)  = New_cmt(Data,p);
OnesIndex=ones(Initial_Size_Data,1);
%Second Step = Sorts Data Accordint to the centre
[~, index]= sort(MinkDist(Data, MinkCentre(OnesIndex,:), p, EqualWeights,OnesIndex));
Data = Data(index,:);

%Third Step Anomalous Patter
while ~isempty (Data)
    DataSize = size(Data,1);
    OnesIndex=ones(DataSize,1);
    TentCentroid = Data(DataSize,:); % Gets a tentative Centroid    
    PreviousBelongsToCentroid=[];
    PreviousPreviousBelongsToCentroid=[];
    TentW=EqualWeights;
    LoopControl=0;
    while LoopControl<500
        %BelongsToCentroid(x,1) = True, if x is closer to the tentative
        %centroid than it is to the Minkowski Centre of the data
        BelongsToCentroid = MinkDist(Data, TentCentroid(OnesIndex,:), p, TentW.^p,OnesIndex) < MinkDist(Data, MinkCentre(OnesIndex,:), p, TentW.^p,OnesIndex);
        NewCentroid(1,:)=New_cmt(Data(BelongsToCentroid==1,:),p);
        if sum(BelongsToCentroid)==0,BelongsToCentroid(size(Data,1),1)=1;end;
        
        %Checks for stop conditions, including cycles (the latter shouldn't
        %happen)
        if isequal(TentCentroid, NewCentroid), break; end
        if isequal(BelongsToCentroid, PreviousBelongsToCentroid), break; end
        if isequal(BelongsToCentroid, PreviousPreviousBelongsToCentroid), break; end
        
        TentCentroid = NewCentroid;
        PreviousBelongsToCentroid = BelongsToCentroid;
        PreviousPreviousBelongsToCentroid = PreviousBelongsToCentroid ;
        
        TentW = GetNewW(Data, BelongsToCentroid, NewCentroid, p,N);
        LoopControl=LoopControl+1;
    end
    if sum(BelongsToCentroid==1)> ikThreshold
        Centroids = [Centroids; NewCentroid]; %#ok<AGROW>
        Weights = [Weights; TentW]; %#ok<AGROW>    
        QtdInCluster = [QtdInCluster; nansum(BelongsToCentroid)]; %#ok<AGROW>
    end
    Data(BelongsToCentroid==1,:)=[];
end

if exist('MaxK','var')
    if size(Centroids,1)>MaxK
        [~,index] = sort(QtdInCluster,'descend');
        Centroids = Centroids(index(1:MaxK),:);
        Weights = Weights(index(1:MaxK),:);
    end
end

InitW=Weights;
InitZ=Centroids;

%Runs MWK-Means if the found initial values
[U, FinalW, FinalZ, UDistToZ, LoopCount] = MWKmeans(InitialData, size(Centroids,1), p, Centroids, Weights);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GetNewW calculates a new set of weights
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function W = GetNewW(Data, U, Z, p, N)
K=size(Z,1);
D = zeros(K,N);
W = D;
for l = 1 : K
    for j = 1 : N    
        D(l,j) = nansum(abs(Data(U==l,j)- Z(l,j)).^p);
    end
end
D = D + 0.01;
%Calculate the actual Weight
%for each column
if p~=1
    exp = 1/(p-1);
    for l = 1 : K
        for j = 1 : N
            tmpD=D(l,j);
            W(l,j)= 1/nansum((tmpD(1,ones(1,N))./D(l,:)).^exp);
        end
    end
else
    for l = 1 : K
        [~, MinIndex] = nanmin(D(l,:));
        W(l,MinIndex)=1;
    end
end





function [U, W, Z, UDistToZ, LoopCount] = MWKmeans(Data, K, p, InitialCentroids, InitialW)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Minkowski Weighted K-Means
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%From:
%Amorim, R.C. and Mirkin, B., Minkowski Metric, Feature Weighting and Anomalous Cluster Initialisation in K-Means Clustering,
%Pattern Recognition, vol. 45(3), pp. 1061-1075, 2012.
%
%Parameters:
%Data
%      Dataset, format: Entities x Features
%K
%      Total number of clusters in the dataset
%p
%      Minkowski and weight exponent
%InitialCentroids (optional)
%      Initial centroids the algorithm should use, format: K x Features.
%InitialW (Optional)
%      Initial set of weights the algorithm should use, format: K x
%      Features.
%
%Outputs
%
%U
%      Cluster Labels. Clearly they may not directly match the dataset
%      labels (you should use a confusion matrix).
%W
%      Final Weights
%Z
%      Final Centroids
%UDistToZ
%      The distance of each entity to its centroid.
%LoopCount
%      The number of loops the algorithm took to converge. The maximum is
%      hard-coded to 500 (variable MaxLoops)

%M Lines and N Columns
[M,N] = size(Data);
MaxLoops = 500; %just in case, shouldn't be necessary

%Binary Variable M x k, cluster label
U = zeros(M, 1);
OldU = U;

% Step 1
%Get Random Initial Centroids (VALUES) X Number of K
if exist('InitialCentroids','var')
    Z = InitialCentroids;
else
    rng(sum(100*clock));
    ObservedData = Data(sum(isnan(Data),2),:)
    Z=datasample(ObservedData,K, 'replace', false);
end

%Generate Initial set of weights
if exist('InitialW','var')
    W = InitialW;
else
    W(1:K,1:N)=1/N;
end

LoopCount = 0;
while LoopCount<=MaxLoops
    %Step 2
    %Find Initial U that is minimized for the initials Z and W
    [NewUtp1, UDistToZ]= MWKGetNewU (Data, Z, W.^p, p,M, K);
    %If there is no alteration in the labels stop
    if isequal(NewUtp1, U), break, end;
    
    %if the labes are equal to the previous-previous lables - stop (cycle)
    %This shouldn't happen
    if isequal(NewUtp1, OldU), break; end;
    
    %Step 3
    OldU = U;
    U = NewUtp1;
    %Get New Centroids
    Ztp1 = MWKGetNewZ(Data, U, K,p, Z);
    %If there is no alteration in the centroids stop
    if isequal(Ztp1,Z), break, end;
    Z = Ztp1;
    %Step 4
    %Update the Weights
    W = MWKGetNewW(Data, U, Z, p,N, K);
    LoopCount = LoopCount + 1;
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GetNewU calculates the labels (clustering)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [U, UDistToZ]= MWKGetNewU (Data, Z, W, p, M, K)
temp_distances=zeros(M,K);
OnesIndex(1:M,1)=1;
%for each Centroid, gets the distance, line = entity, col = cluster
for c = 1 : K
    tmp_Z=Z(c,:);
    temp_distances(:,c) = MinkDist(Data, tmp_Z(OnesIndex,:), p, W(c,:),OnesIndex);
end
[UDistToZ, U]=nanmin(temp_distances,[],2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GetNewZ calculates the centroids
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Z = MWKGetNewZ(Data, U, K,p, OldZ)
Z = OldZ;
for l = 1 : K
    if sum(U==l)>1
        %if there isnt any entity in the cluster (shouldnt be the case!) = dont change the Z
        Z(l,:)=New_cmt(Data(U==l,:),p);
    end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%GetNewW calculates the new set of weights
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function W = MWKGetNewW(Data, U, Z, p,N, K)
D = zeros(K,N);
W=D;
%Calculates the dispersion of each feature at each cluster
for l = 1 : K
    for j = 1 : N
        D(l,j) = nansum(abs(Data(U==l,j)- Z(l,j)).^p);
    end
end
D = D + nanmean(mean(D));
%Calculates the actual feature weights
if p~=1
    exp = 1/(p-1);
    for l = 1 : K
        for j = 1 : N
            tmpD=D(l,j);
            W(l,j)= 1/nansum((tmpD(1,ones(N,1))./D(l,:)).^exp);
        end
    end
else
    for l = 1 : K
        [~, MinIndex] = nanmin(D(l,:));
        W(l,MinIndex)=1;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MinkDist calculates the Minkowski distance (in fact, the weighted pth root
% of the Minkowski distance, between the data set x and y
% w contains the weights
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function r= MinkDist(x, y, p, w,OnesIndex)
%calculates the  Minkowski distance in between x and y
r = nansum((abs(x - y).^p).*w(OnesIndex,:),2).^(1/p);





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New_cmt calculates the Minkowski centre of the data at the Minkowski
% exponent p
% Data = Entitiex x Features (features = columns)
% It follows the procedure described at:
% Amorim, R.C., Hennig, C., Recovering the number of clusters in data sets with noise features using feature rescaling factors, Information Sciences, Elsevier, vol. 324, pp. 126-145, 2015.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [DataCenter]=New_cmt(Data,p)
%Data should be EntityxFeatures and standardised.
%Calculates ALL centers rather than one at a time
[N,M]=size(Data);
if p==1
    DataCenter=nanmedian(Data,1);
    return;
elseif p==2
    DataCenter=nanmean(Data,1);
    return;
elseif N==1
    DataCenter=Data;
    return;
end
Gradient(1,1:M)=0.001;
OnesIndex(1:N,1)=1;
DataCenter = nansum(Data,1)./N;
DistanceToDataCenter=nansum(abs(Data - DataCenter(OnesIndex,:)).^p);
NewDataCenter=DataCenter+Gradient;
DistanceToNewDataCenter=nansum(abs(Data - NewDataCenter(OnesIndex,:)).^p);
Gradient(1,DistanceToDataCenter < DistanceToNewDataCenter) = Gradient(1,DistanceToDataCenter < DistanceToNewDataCenter).*-1;
while true
    NewDataCenter = DataCenter + Gradient;
    DistanceToNewDataCenter=nansum(abs(Data - NewDataCenter(OnesIndex,:)).^p);  
    Gradient(1,DistanceToNewDataCenter>=DistanceToDataCenter)=Gradient(1,DistanceToNewDataCenter>=DistanceToDataCenter).*0.9;
    DataCenter(1,DistanceToNewDataCenter<DistanceToDataCenter)=NewDataCenter(1,DistanceToNewDataCenter<DistanceToDataCenter);
    DistanceToDataCenter(1,DistanceToNewDataCenter<DistanceToDataCenter)=DistanceToNewDataCenter(1,DistanceToNewDataCenter<DistanceToDataCenter);    
    if all(abs(Gradient)<0.0001), break, end;
end

