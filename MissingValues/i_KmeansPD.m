function [Centroids QtdEntitiesInCluster] = i_KmeansPD(stdData, MinEntitiesInCluster, IsstdDataStandarized, k)
%First Step = Standarize stdData if needed
InitialSize = size(stdData,1);
QtdEntitiesInCluster = [];

if IsstdDataStandarized == false
 r = stdData - repmat(nanmean(stdData), InitialSize ,1);
 stdData = r./repmat(nanmax(stdData) - nanmin(stdData),InitialSize , 1);
end

%Second Step = Sorts stdData Accordint to Distance to Zero
[desc,index] = sort(sum(stdData.^2,2));
stdData = stdData(index,:);

%Third Step Anomalous Patter
Centroids = [];
while ~isempty (stdData)
 CurrentSize = size(stdData,1);
 TentCentroid = stdData(CurrentSize,:); % Gets a tentative Centroid
 DistanceToCentre = sum(stdData.^2,2);
 while true
 BelongsToCentroid = sum((stdData-(repmat(TentCentroid, CurrentSize,1))).^2,2) < DistanceToCentre;
 NewCentroid = mean(stdData(BelongsToCentroid==1,:),1);
 if isequal(TentCentroid, NewCentroid), break; end
 TentCentroid = NewCentroid;
 end
 if size(find(BelongsToCentroid==1),1) > MinEntitiesInCluster
 Centroids = [Centroids; NewCentroid]; %#ok<AGROW>
 QtdEntitiesInCluster = [QtdEntitiesInCluster; sum(BelongsToCentroid==1)];
 end
 stdData(BelongsToCentroid==1,:)=[];
end

if nargin == 4 && size(Centroids,1) > k
 %Gets the k most populated clusters
 [s,ind] = sort(QtdEntitiesInCluster,'descend');
 Centroids = Centroids(ind(1:k),:);
end

