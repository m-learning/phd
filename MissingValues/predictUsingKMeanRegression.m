function [ missingData,missIndexCol,ABS_DIFF_INDEX,RAD_INDEX,RAND_INDEX] = predictUsingKMeanRegression( missingData,varargin)
% % % clear
% % % load('RealDataSet/REAL_Iris_150x4_3_std.mat')
% % % [missingData, missIndexCol11]    = getMissingValue(Data{1,1},0.1);
% % % K = 3;


    %% parse variable.....
    %% get label, K and missingListCol from the parameters...
     for i=1:2:nargin-1

         varName = varargin{i};
         switch varName
             case 'label'
                 label = varargin{i+1};
             case 'K'
                 K = varargin{i+1};   
             case 'missingListCol'
                 missIndexCol = varargin{i+1};   
             case 'InitMethod'
                 initMethod = varargin{i+1};
             case 'kMeanType'
                 kMeanType = varargin{i+1};
         end
     end

     

    %% get the cardinality of current data set
    [N,M] = size(missingData);    
    %% get the number of total missing values
 %  TOTAL_MISSING = sum(sum(isnan(missingData)));
    
     if ~exist('missIndexCol', 'var')
         MISS_COUNT = sum(sum(isnan(missingData)));
         missIndexCol = NaN(MISS_COUNT,3);
         [missIndexCol(:,1), missIndexCol(:,2)]= find(isnan(missingData));
     end
     
    
     
     missIndexCol = [missIndexCol NaN(size(missIndexCol,1),1)];
     mDataInitPred = missingData;
     if initMethod == InitConstants.USE_AVERAGE
        %% replace the missing values average feature value
        [mDataInitPred,~] = predictUsingAverageVal(missingData);
     elseif initMethod == InitConstants.USE_REGRESSION
         [ mDataInitPred,~ ] = predictUsingSRegression( missingData);
     end
     
     if kMeanType == KMEAN_TYPE.KMEAN_ORGINAL
        %% cluster the data set using k-means
        [U,~] = kmeans(mDataInitPred,K,'Replicates',100);
     elseif kMeanType == KMEAN_TYPE.KMEAN_IK
         %% minimum number of element in cluster is set to 2....
          MIN_ENTITY = 2; 
            %% data is not scaled
            [centorids, ~] = i_Kmeans(mDataInitPred,MIN_ENTITY, false);
            %predU = getStraightKMean(missingDataAvg,false,'centroids',centorids);
            U = getStraightKMean(mDataInitPred,false,'centroids',centorids);
     end
    %% map each labels with their position in original data set( not in their corresponding cluster) with their cluster ID.
    LABEL_MAPPING = [(1:N)' U];
    
    workList=[];
        
    %% for each cluster....
    for u=unique(U)'
        %% get the current cluster's label mapping with their position in the orginal data set....
        %% [ original_data_id current_Cluster_ID]
        currMapping = LABEL_MAPPING(LABEL_MAPPING(:,2)==u,:);
        %% get the missing data within current cluster        
        %% [orginal_data_id feature_id attribute_value]
        %% check if any of the orginal_data_id from the current Mapping are in the missingList collection...
        currMListCol = missIndexCol(ismember(missIndexCol(:,1),currMapping(:,1),'rows'),:);
        
        if size(currMListCol,1) == 0 
            disp('No Missing values.............')
        else


            %% get the orginal_data_id for the data in current cluster......
       %%     [MAP_INDEX,~]=find(U==u);

            %% get the cluster data
            currData = missingData(U==u,:);

       %     tmpMissInfo = missingData(U==u && isnan(sum(isnan(missingData),2)) > 0,:);

            %% find the rowId and column Id for the missing data in current cluster....
        %    [rM,cM]= find(isnan(currData));
           %% currCleanData = currData(setdiff(1:size(currData,1),rM'),:);


               [currCleanData,~] = predictUsingAverageVal(currData);
               %% in case where cluster have only one element there may be case that currCleanData have some NaN value. In such case we replace the NaN with
               %% average Feature value

               kk = currCleanData;
               kk1 = missingData;
               if sum(sum(isnan(currCleanData)))> 0
                   avgFeatureValue = nanmean(missingData);
                   [rM,cM]=find(isnan(currCleanData));

                   %% in case when there is only one row, find returns rows and columns in only one row so need to revert it
                   if size(currCleanData,1) == 1
                       rM = rM';
                       cM = cM';
                   end

                   for rowId = 1:size(rM,1)

                           currCleanData(rM(rowId,1),cM(rowId,1))=avgFeatureValue(1,cM(rowId,1));

                   end
               end
               fprintf('\nMissing data in current clear Data %d',sum(sum(isnan(currCleanData))));
               if sum(sum(isnan(currCleanData)))
                   disp('error here');
               end
               for missIndex = 1:size(currMListCol)
                      currMRow = currMListCol(missIndex,1); %% added
                      currMCol = currMListCol(missIndex,2); %% added
                      currCCols = setdiff(1:M,currMCol); 
                      try
                       b = regress(currCleanData(:,currMCol),currCleanData(:,currCCols));   %%% added 

                            currMRowId = find(currMapping(:,1) == currMRow);
                            X = currCleanData(currMRowId,currCCols);
                       catch

                 %%      disp('total X');
                            sum(sum(isnan(X)));
                       end
                       missingData(currMRow,currMCol)= X*b;                   %% addded...
                       missIndexCol(missIndexCol(:,1)==currMRow & missIndexCol(:,2)==currMCol,4) = X*b;
                       workList=[workList;currMRow currMCol];
               end
        end


    end
 
    ABS_DIFF_INDEX = NaN;
    RAD_INDEX = NaN;
    RAND_INDEX = NaN;
     if ~isnan(missIndexCol(1,3))    
        ABS_DIFF_INDEX = sum(abs(missIndexCol(:,3)-missIndexCol(:,4)));
        RAD_INDEX = sum(abs((missIndexCol(:,3)-missIndexCol(:,4))./(missIndexCol(:,3))))/size(missIndexCol,1);
     end 
    if exist('label', 'var')     
        sum(sum(isnan(missingData)))
         try 
        [U,~] = kmeans(missingData,K,'Replicates',100);  %% added.....
         
        RAND_INDEX = RandIndex(label,U); %% added.....      
        catch
             workList
         end
    end 
    end
 


