function [U, FinalW, InitW, FinalZ, InitZ, UDistToZ,LoopCount] = SubWIkMeansWithMValue(Data, ikThreshold, Beta, MaxK, p, RunOnlyInit)
%
%From:
%Amorim, R.C. and Mirkin, B., Minkowski Metric, Feature Weighting and Anomalous Cluster Initialisation in K-Means Clustering, 
%Pattern Recognition, vol. 45(3), pp. 1061-1075, 2012.
%
%Parameters:
%Data
%      Dataset, format: Entities x Features
% ikThreshold
%       The intelligent K-Means threshold (theta). If the number of
%       clusters is known set this to zero.
%Beta 
%      Weight Exponent. If you are running the intelligent Minkowski weighted k-means
%      the value for beta should be the same as for p.
%MaxK
%       The number of clusters. 
%p
%       The Minkowski exponent p. If you are running the intelligent
%       Minkowski weighted k-means this value should be the same as Beta
%
%RunOnlyInit
%       Set this to true if all you want is to get initial centroids. For a
%       complete clustering, set this to false.
%
%REMARKS
%      If you want to use the intelligent MWK-Means as in the paper, the value of Beta
%      and p should be the same.
%      If you know the number of clusters, set iKThereshold to 0 and MaxK to the number of clusters.
%      
%
%Outputs
%
%U
%      Cluster Labels. Clearly they may not directly match the dataset
%      labels (you should use a confusion matrix).
%FinalW
%      Final Weights
%InitW
%      Initial Weights
%FinalZ
%      Final Centroids
%InitZ
%      Initial Centroids
%UDistToZ
%      The distance of each entity to its centroid.
%LoopCount
%      The number of loops the algorithm took to converge. The maximum is
%      hard-coded to 500 (variable MaxLoops)
Data = (Data - repmat(nanmean(Data),size(Data,1),1))./(repmat(nanmax(Data)-nanmin(Data),size(Data,1),1));
[Initial_Size_Data,N] = size(Data);
Weights = [];
QtdInCluster = [];
Centroids = []; 
EqualWeights(1,1:N)=1/N;
if ~exist('RunOnlyInit', 'var'), RunOnlyInit=false; end
InitialData = Data;

%Calculates the Minkowski Centre if necessary
%Second Step = Sorts Data Accordint to Distance to Zero
if exist('p','var')
    MinkCentre(1,:)  = New_cmt(Data,p); 
    OnesIndex=ones(Initial_Size_Data,1);
    [~, index]= sort(MinkDist(Data, MinkCentre(OnesIndex,:), p, EqualWeights,OnesIndex)); 
    RunMWK=true;
else
    RunMWK=false;
    [~, index] = sort(nansum(Data.^2,2));
end
Data = Data(index,:);

%Third Step Anomalous Patter
while ~isempty (Data)
    DataSize = size(Data,1);
    OnesIndex=ones(DataSize,1);
    TentCentroid = Data(DataSize,:); % Gets a tentative Centroid    
    PreviousClusterSize = 0;
    PreviousBelongsToCentroid=[];
    PreviousPreviousBelongsToCentroid=[];
    TentW=EqualWeights;
    LoopControl=0;
    while LoopControl<500
        if ~RunMWK
            BelongsToCentroid =  nansum(((Data-(repmat(TentCentroid, DataSize ,1))).^2).*repmat(TentW.^Beta, DataSize ,1),2) < nansum((Data.^2).*repmat(TentW.^Beta,DataSize ,1),2);
            NewCentroid = nanmean(Data(BelongsToCentroid==1,:),1);
        else
            %uses anomalous pattern, get the centre
            BelongsToCentroid = MinkDist(Data, TentCentroid(OnesIndex,:), p, TentW.^Beta,OnesIndex) < MinkDist(Data, MinkCentre(OnesIndex,:), p, TentW.^Beta,OnesIndex);
            NewCentroid(1,:)=New_cmt(Data(BelongsToCentroid==1,:),p);
            if nansum(BelongsToCentroid)==0,BelongsToCentroid(size(Data,1),1)=1;end;
        end

        if isequal(TentCentroid, NewCentroid), break; end
        if isequal(BelongsToCentroid, PreviousBelongsToCentroid), break; end
        if isequal(BelongsToCentroid, PreviousPreviousBelongsToCentroid), break; end
        
%        if sum(BelongsToCentroid) < PreviousClusterSize 
%            NewCentroid = TentCentroid; %The previous one with a bigger cluster
%            BelongsToCentroid = PreviousBelongsToCentroid;
%            break;
%        end 
        TentCentroid = NewCentroid;
        PreviousClusterSize = nansum(BelongsToCentroid);
        PreviousPreviousBelongsToCentroid = PreviousBelongsToCentroid ;
        PreviousBelongsToCentroid = BelongsToCentroid;
        TentW = GetNewW(Data, BelongsToCentroid, NewCentroid, Beta,p, RunMWK,N);
        LoopControl=LoopControl+1;
    end 
    if nansum(BelongsToCentroid==1)> ikThreshold
        Centroids = [Centroids; NewCentroid]; %#ok<AGROW>
        Weights = [Weights; TentW]; %#ok<AGROW>    
        QtdInCluster = [QtdInCluster; nansum(BelongsToCentroid)]; %#ok<AGROW>
    end
    Data(BelongsToCentroid==1,:)=[];
end

if nargin>=4 && size(Centroids,1)>MaxK
    [~,index] = sort(QtdInCluster,'descend');
    Centroids = Centroids(index(1:MaxK),:);
    Weights = Weights(index(1:MaxK),:);
end

InitW=Weights;
InitZ=Centroids;

if nargin<=4
    [U, FinalW, FinalZ, UDistToZ, LoopCount] = SubWkMeansWithMValue (InitialData, size(Centroids,1), Beta, Centroids, InitW);
elseif RunOnlyInit==false
    [U, FinalW, FinalZ, UDistToZ, LoopCount] = SubWkMeansWithMValue (InitialData, size(Centroids,1), Beta, Centroids, InitW, p);
else
    %Running the Initialization Only - no need for SubWkMeans
    FinalW=[];
    FinalZ=[];
    UDistToZ=[];
    LoopCount=[];
    U=[];
end


function W = GetNewW(Data, U, Z, Beta, p, RunMWK, N)
k = size(Z,1);
D = zeros(k,N);
W = D;
for l = 1 : k
    for j = 1 : N    
      if RunMWK
          D(l,j) = nansum(abs(Data(U==l,j)- Z(l,j)).^p);
      else
          D(l,j) = nansum((Data(U==l,j)- Z(l,j)).^2);
      end
    end
end
D = D + 0.01;%mean(mean(D)); %adds

%Calculate the actual Weight
%for each column
if Beta~=1
    exp = 1/(Beta-1);
    for l = 1 : k
        for j = 1 : N
            tmpD=D(l,j);
            W(l,j)= 1/nansum((tmpD(1,ones(1,N))./D(l,:)).^exp);
        end
    end
else
    for l = 1 : k
        [~, MinIndex] = nanmin(D(l,:));
        W(l,MinIndex)=1;
    end
end 

function r= MinkDist(x, y, p, w, OnesIndex)
%calculates the  Minkowski distance in between x and y
r = nansum((abs(x - y).^p).*w(OnesIndex,:),2).^(1/p);


function [DataCenter]=New_cmt(Data,p)
%Calculates the Minkowski center at a given p.
%Data MUST BE EntityxFeatures and standardised.
[N,M]=size(Data);
if p==1
    DataCenter=nanmedian(Data,1);
    return;
elseif p==2
    DataCenter=nanmean(Data,1);
    return;
elseif N==1
    DataCenter=Data;
    return;
end
Gradient(1,1:M)=0.001;
OnesIndex(1:N,1)=1;
DataCenter = nansum(Data,1)./N;
DistanceToDataCenter=nansum(abs(Data - DataCenter(OnesIndex,:)).^p);
NewDataCenter=DataCenter+Gradient;
DistanceToNewDataCenter=nansum(abs(Data - NewDataCenter(OnesIndex,:)).^p);
Gradient(1,DistanceToDataCenter < DistanceToNewDataCenter) = Gradient(1,DistanceToDataCenter < DistanceToNewDataCenter).*-1;
while true 
    NewDataCenter = DataCenter + Gradient;
    DistanceToNewDataCenter=nansum(abs(Data - NewDataCenter(OnesIndex,:)).^p);  
    Gradient(1,DistanceToNewDataCenter>=DistanceToDataCenter)=Gradient(1,DistanceToNewDataCenter>=DistanceToDataCenter).*0.9;
    DataCenter(1,DistanceToNewDataCenter<DistanceToDataCenter)=NewDataCenter(1,DistanceToNewDataCenter<DistanceToDataCenter);
    DistanceToDataCenter(1,DistanceToNewDataCenter<DistanceToDataCenter)=DistanceToNewDataCenter(1,DistanceToNewDataCenter<DistanceToDataCenter);    
    if all(abs(Gradient)<0.0001), break, end;
end





    