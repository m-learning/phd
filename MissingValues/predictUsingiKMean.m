%[ missingData,missIndexCol_4,ABS_DIFF_INDEX(1,4),RAD_INDEX(1,4),RAND_INDEX(1,4)] = predictUsingiKMean(dataWithMissVal,'missingListCol',missingListCol,'InitMethod',InitConstants.USE_AVERAGE,'label',label,'Version',1);
%[ missingData,missIndexCol_5,ABS_DIFF_INDEX(1,5),RAD_INDEX(1,5),RAND_INDEX(1,5)] = predictUsingiKMean(dataWithMissVal,'missingListCol',missingListCol,'InitMethod',InitConstants.USE_AVERAGE,'label',label,'Version',2);
                
function [  tmpMissingData,missIndexCol,ABS_DIFF_INDEX,RAD_INDEX,RAND_INDEX] = predictUsingiKMean( missingData,varargin )
    %% parse variable.....
    %% get label, K and missingListCol from the parameters...
     for i=1:2:nargin-1

         varName = varargin{i};
         switch varName
             case 'label'
                 label = varargin{i+1};            
             case 'missingListCol'
                 missIndexCol = varargin{i+1}; 
             case 'MIN_ENTITY'
                 MIN_ENTITY = varargin{i+1};
             case 'InitMethod'
                 initMethod = varargin{i+1};  
             case 'Version'
                 VERSION = varargin{i+1};
             case 'K'
                 K = varargin{i+1};
         end
     end
     %%% parameter initialization
     
     if ~exist('MIN_ENTITY','var')
         MIN_ENTITY = 2;
     end
     
     if ~exist('VERSION','var')
         VERSION = 1;
     end
     
     if ~exist('missIndexCol', 'var')
         MISS_COUNT = sum(sum(isnan(missingData)));
         missIndexCol = NaN(MISS_COUNT,3);
         [missIndexCol(:,1), missIndexCol(:,2)]= find(isnan(missingData));
     end
     
     
     
     
     if ~exist('initMethod','var')
         initMethod = NaN;
     end
                    
                 
     
     if initMethod == InitConstants.USE_AVERAGE
         [ tmpMissingData,~ ] = predictUsingAverageVal(missingData);
     elseif initMethod == 11%InitConstant.KMEAN_ORGINAL
         [tmpMissingData,~] = predictUsingKMean(missingData,K,'missingListCol',missIndexCol(:,1:3),'InitMethod',InitConstants.USE_AVERAGE,'Version',1);
     else
         tmpMissingData = missingData;
     end
     
     missIndexCol = [missIndexCol NaN(size(missIndexCol,1),1)];
     
    %% minimum number of element in cluster is set to 2....
    %% data is not scaled
   
    %%% Main Algorithm
    
    if ~isnan(initMethod)
        [centroids, ~] = i_Kmeans(tmpMissingData,MIN_ENTITY, false);
        % predU = getStraightKMean(tmpMissingData,false,'centroids',centorids);
        predU = kmeans(tmpMissingData, size(centroids,1), 'Start', centroids)
    else
        [avgPredData,~] = predictUsingAverageVal(missingData);
        [centroids, ~] = i_Kmeans(avgPredData,MIN_ENTITY, false);
        predU = kmeans(avgPredData, size(centroids,1), 'Start', centroids);
      
    end
     
    tmpMissingDataOld = tmpMissingData;
    
    %% Replace the missing data with mean value within cluster....
  %  [mRowIndex, mColIndex]=find(isnan(missingData));
  %  for missIndex = 1:size(mRowIndex,1)
     for missIndex =1:size(missIndexCol,1)
        rN = missIndexCol(missIndex,1);
        rM = missIndexCol(missIndex,2);
        %%% synchronize for synchronize, all the effect should happen in
        %%% same run so for each missing data we take from old data
        if VERSION == 1
            currCluster = tmpMissingDataOld(predU==predU(rN,1),rM);
            tmpMissingData(rN,rM) = nanmean(currCluster);
        elseif VERSION == 2
            currCluster = tmpMissingData(predU==predU(rN,1),rM);
            tmpMissingData(rN,rM) = nanmean(currCluster);
        end
        missIndexCol(missIndex,4) = nanmean(currCluster);
     end
    
    
     %% when there is no initialization of missing values there are chances that some of the predict values get NaN, when all the attributes values wihtin the cluster are missing
     %% in such case we again run the process with increasing the size of cluster
     newMissIndex = missIndexCol(isnan(missIndexCol(:,4)),1:3);
     if (size(newMissIndex,1)>0)
         fprintf('\nMissing Value count : %d ',size(newMissIndex,1));
         newMinEntity = min(2*MIN_ENTITY,size(tmpMissingData,1));
         [ tmpMissingData,newMissIndexCol,~]=predictUsingiKMean( tmpMissingData,'missingListCol',newMissIndex,'MIN_ENTITY', newMinEntity);
         for i=1:size(newMissIndexCol)
             missIndexCol(missIndexCol(:,1)==newMissIndexCol(i,1) && missIndexCol(:,2)==newMissIndexCol(i,2),4) = newMissIndexCol(i,4);
         end
     end
    
%     %% in version 2 there is chances that all of the attribute values withing the cluster are missing
%         %% Therefore, we chack if any of the predicted value is nan...
%         %% if yes, we repalce the value with average feature value.
%         
%             
%          if VERSION == 2
%             if sum(sum(isnan(missingData))) > 0
%                 disp('still have some NaN')
%                 avgFeatureValue = nanmean(missingData);
%                 [mRow mCol] = find(isnan(missingData));
% 
%                 for mIndex=1:size(mRow,1)
%                     missingData(mRow(mIndex,1),mCol(mIndex,1))=avgFeatureValue(1,mCol(mIndex,1));
%                     missIndexCol(missIndexCol(:,1)==mRow(mIndex,1) & missIndexCol(:,2) == mCol(mIndex,1),4)= avgFeatureValue(1,mCol(mIndex,1));
%                     %% synchronize average feature value with each updata....
%                     avgFeatureValue = nanmean(missingData);
%                 end
%             end
%             tmpMissingData = missingData;
%          end
         
        
        
    %%%%%%%%%%%%%% Performance meaurement
        
    if exist('label','var')
        [U,~] = kmeans(tmpMissingData,max(label),'Replicates',100);
        RAND_INDEX=RandIndex(label,U);
    else
        RAND_INDEX = NaN;
    end
    
    %% find the RAD index by comparing the predict value with the orginal value given in missIndexCol [rowId,colId,actualValue]
    if ~isnan(missIndexCol(1,3)) 
        ABS_DIFF_INDEX = sum(abs(missIndexCol(:,3)-missIndexCol(:,4)));
        RAD_INDEX = (sum(abs((missIndexCol(:,3)-missIndexCol(:,4))./missIndexCol(:,3))))/size(missIndexCol,1);
    else
        ABS_DIFF_INDEX=NaN;
        RAD_INDEX = NaN;
    end

 
end


