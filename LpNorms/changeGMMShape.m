function [ dataNew,Angle ] = changeGMMShape( data,U,p,pOld,Angle)
%CHANGEGMMSHAPE0 Summary of this function goes here
%   Detailed explanation goes here

[N,M]=size(data);
K = max(U);


if isempty(Angle)
    Angle=zeros(N,M-1);
    for k=1:K
        currList = find(U==k);
        tmpAngle = getAngle(data(currList,:),pOld);
        Angle(currList,:) = tmpAngle;
    end
end

dataNew = data;

for k=1:K
    currList = find(U==k);   
    dataNew(currList,:) = changeClusterShapeMDNew(data(currList,:),p,pOld,Angle(currList,:));
end

end