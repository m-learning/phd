function [ NewSignedDataAtCentroid ] = getNDimAlterClusterShape( data,U,p )

dataAtCenter = data;
[N,M]=size(data);
%%get centroids
K = max(U);

centroid=nan(K,M);
for k=1:K
    centroid(k,:) = mean(data(U==k,:));
    %% move data to (0,0,0).
    nk = sum(U==k);
    dataAtCenter(U==k,:)=dataAtCenter(U==k,:)-repmat(centroid(k,:),nk,1);
end



% for k=1:max(U)
%     nK = sum(U==k)
%     dataAtCenter(U==k,:)=dataAtCenter(U==k,:)-repmat(mean(dataAtCenter(U==k,:)),nK,1);
% end

%% move all data points to the positive plane
absDataAtCenter = abs(dataAtCenter);
r = (sum(absDataAtCenter.^2,2)).^(0.5);
theta=NaN(N,M-1);
%% finding angle...
for i=1:N
    for j=1:M-1
        currR = (sum(absDataAtCenter(i,j:M).^2)).^(0.5);
        %theta(i,j)=acosd(absDataAtCenter(i,j)/r(i,1));
        theta(i,j)=acosd(absDataAtCenter(i,j)/currR);
        
    end
end

theta = (theta*pi)/180;
%%% find r new

rNew = r;
for i=1:N
    lower=cos(theta(i,1))^p;
    txtlower = 'acosd(theta1)';
    
    for j=2:M
        currLower=1;
      
        for jj = 1:j-1
            currLower = currLower*sin(theta(i,jj))^p;
           
        end
        if j~=M
            currLower = currLower*cos(theta(i,j))^p;
           
        end
        
        lower = lower+  currLower;
      
        if lower == 0
            lower = 0.00000000000001;
        end
        
    end
    %disp(txtlower)
    rNew(i,1) = r(i,1)/(lower).^(1/p);
end

absNewDataAtCenter = absDataAtCenter;
for i=1:N
    lower=cos(theta(i,1));
    
    %disp(txtlower)
    absNewDataAtCenter(i,1)=rNew(i,1)*lower;
    for j=2:M
        currLower=1;
        txtCurrLower='';
        for jj = 1:j-1
            currLower = currLower*sin(theta(i,jj));
           
        end
        if j~=M
            currLower = currLower*cos(theta(i,j));
            
        end
        
        %disp(txtCurrLower)
        absNewDataAtCenter(i,j)=rNew(i,1)*currLower;
        %lower = lower+  currLower;
        %txtlower = strcat(txtlower,'+',txtCurrLower);
        
        
    end
    
    
end


%% Move the data point to their respective plane
signPositive = dataAtCenter>0;
signNegative = dataAtCenter<0;
sign = signPositive - signNegative;

NewSignedDataAtCenter=absNewDataAtCenter.*sign;

NewSignedDataAtCentroid = NewSignedDataAtCenter;
for k=1:K
    nk = sum(U==k);
    NewSignedDataAtCentroid(U==k,:) = NewSignedDataAtCenter(U==k,:)+repmat(centroid(k,:),nk,1);
end


end

