function [ newPoints] = changeClusterShapeMDNew( points,p,pOld,Angle)

newPoints = points;



%centroids = mean(newPoints,1);
centroids = New_cmt(newPoints,pOld);
N = size(points,1);
M=size(points,2);
newPoints = newPoints-repmat(centroids,N,1);

%% move all data points to the positive plane


% %% Move the data point to their respective plane
signPositive = newPoints>0;
signNegative = newPoints<0;
sign = signPositive - signNegative;

newPoints=newPoints.*sign;

for counter = 1:size(points,1)
    
    theta = Angle(counter,:);
    
    
    r=(sum(newPoints(counter,:).^pOld)).^(1/pOld);
%     rChk=nan;
%     if M==2
%         rChk = (newPoints(counter,1)^pOld+newPoints(counter,2)^pOld)^(1/pOld);
%     elseif M==3
%         rChk = (newPoints(counter,1)^pOld+newPoints(counter,2)^pOld +  newPoints(counter,3)^pOld   )^(1/pOld);
%     end
     
    
    try
        % lower = (sin(theta)).^p+(cos(theta)).^(p);
        
        
        lowerRNew=cos(theta(1,1))^p;
        for j=2:M
            tmpLowerR =1;
            for jj=1:j-1
                tmpLowerR = tmpLowerR*sin(theta(1,jj))^p;
            end
            if j~=M
                tmpLowerR=tmpLowerR*(cos(theta(1,j))^p);
            end
            lowerRNew=lowerRNew+tmpLowerR;
        end
        
        
        %
        %         end
        
%         if M==2
%             lowerChk= (cos(theta(1,1))^p+sin(theta(1,1))^p);
%             rNewChk = rChk./(((lowerChk)).^(1/p));
%         elseif M==3
%             lowerChk= (cos(theta(1,1))^p+(sin(theta(1,1))^p)*(cos(theta(1,2))^p)+(sin(theta(1,1))^p)*(sin(theta(1,2))^p));
%             rNewChk = rChk./(((lowerChk)).^(1/p));
%         end
    catch
        x=1
    end
    
    newR = r./(((lowerRNew)).^(1/p));
    %  [newR rNewChk];
    
    %newR = abs(r^p/((sin(theta))^p+((cos(theta)))^p))^(1/p)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %       chk=newPoints;
    for j=1:M
        tmp=newR(1,1);
        for jj=1:j-1
            tmp=tmp*sin(theta(1,jj));
        end
        if j~=M
            try
                tmp = tmp*cos(theta(1,j));
            catch
                x=1
            end
        end
        newPoints(counter,j)=tmp;
    end
    
%     if M==2
%         x1=rNewChk*cos(theta(1,1));
%         x2=rNewChk*sin(theta(1,1));
%     elseif M==3
%         x1=rNewChk*cos(theta(1,1));
%         x2=rNewChk*sin(theta(1,1))*cos(theta(1,2));
%         
%         x3=rNewChk*sin(theta(1,1))*sin(theta(1,2));
%         
%     end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
%     if M==2
%         newPointChk(counter,:)=[x1 x2];
%     elseif M==3
%         newPointChk(counter,:)=[x1,x2,x3];
%     end
    
    
    
end


% posIndex = find(points(:,4)>0)
% posIndex=posIndex(1:20,1);
% negIndex = find(points(:,4)<0)
% negIndex=negIndex(1:20,1);
% [points(posIndex,4) Angle(posIndex,4) points(negIndex,4) Angle(negIndex,4)] ;
%newPoints(Angle(:,end)>pi/2,end)=newPoints(Angle(:,end)>pi/2,end)*-1;
%[min(Angle(points(:,4)>0,4)) max(Angle(points(:,4)>0,4))]
%x=1;
newPoints=newPoints.*sign;
newPoints = newPoints+repmat(centroids,N,1);




% newPointChk=newPointChk.*sign;
% newPointChk = newPointChk+repmat(centroids,N,1)
% x=1
%newPoints = newPoints+repmat(centroids,N,1);
