% For the experiments in this paper we have generated a total of 600 synthetic
% data sets. We have used 12 different configurations, with and without noise
% features. The noise features are comprised of uniformly random values within
% the domain of the data set. These are pseudo-random values drawn from the
% standard uniform distribution. Each data set has spherical Gaussian clusters
% with diagonal covariance matrices of ?
% 2 = 0.5. All centroid components were independently generated from the Gaussian distribution N(0, 1), and each point
% had a chance of 1/K to come from any cluster.
% We initially generated 50 data sets for each of the following configurations:
% (i) 1000 entities and 8 features partitioned into 2 clusters (1000x8-2); (ii) 1000
% entities and 12 features partitioned into three clusters (1000x12-3); (iii) 1000
% entities and 16 features partitioned into four clusters (1000x16-4); (iv) 1000
% entities and 20 features partitioned into five clusters (1000x20-5), which defines
% a subtotal of 200 data sets
function [ GMM_DataSet,dbFileName,allDD] = generateGMMDataSet( N,fList,kList,DB_SIZE,covMLow,covMHigh,dbName)
    FILE_N = size(fList,2);
    GMM_DataSet=cell(1,FILE_N);
    dbFileName=cell(1,1);
    % each of the configurations 10 0 0 �6 ?3,  10 0 0 �12 ?6 and 10 0 0 �20 ?10
    for fIndex=1:FILE_N
        
        try
            dbFileName{1,fIndex} =strcat(dbName,'_',num2str(N),'x',num2str(fList(1,fIndex)),'_',num2str(kList(1,fIndex)),'_NF');
        catch
            dbFileName = dbName;
        end
        M = fList(1,fIndex);
        K = kList(1,fIndex);
        myDataCol=cell(2,DB_SIZE);
        for dbIndex=1:DB_SIZE
           
           addpath('C:\netlab');
           % Each data set has spherical Gaussian clusters with diagonal
           mix = gmm(M,K,'spherical');
           % Each data set has spherical Gaussian clusters with diagonal         
            mix.covars=zeros(1,K);
            mix.covars(1,:)= covMLow + (covMHigh-covMLow).*rand(1,K);
            for k=1:K            
                %Each of the centroid components was generated independently using the standard normal distribution N (0, 1). 
                mix.centres(k,:)=normrnd(zeros(1,M),1);
            end 
            %% generate data using gmmsamp, 
            [data, U] = gmmsamp(mix, N);    
            %% normalize the data set...
            data = (data - repmat(mean(data),N,1))./repmat(max(data)-min(data),N,1);
            rmpath('C:\netlab');
            %% store dataset        
            myDataCol{1,dbIndex}=data;
            myDataCol{2,dbIndex}=U;
        end
        GMM_DataSet{1,fIndex}=myDataCol;
    end
    
    allDD = zeros(FILE_N,DB_SIZE);
    for fIndex=1:FILE_N
        dd=GMM_DataSet{1,fIndex};
        
        for ii=1:DB_SIZE
            allDD(fIndex,ii)= RandIndex(kmeans(dd{1,ii},kList(1,1)),dd{2,ii});
        end
    end
    
end

